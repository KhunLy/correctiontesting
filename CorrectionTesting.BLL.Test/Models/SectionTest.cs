﻿using CorrectionTesting.BLL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionTesting.BLL.Test.Models
{
    [TestClass]
    public class SectionTest
    {
        private Section s;

        [TestInitialize]
        public void Setup()
        {
            s = new Section();
            s.Add(new Student() 
            { 
                LastName = "Ly", 
                FirstName = "Khun", 
                BirthDate = new DateTime(1982, 5,6), 
                YearResult = 10 
            });
            s.Add(new Student()
            {
                LastName = "Ly",
                FirstName = "Piv",
                BirthDate = new DateTime(1981, 4, 30),
                YearResult = 11
            });
            s.Add(new Student()
            {
                LastName = "Person",
                FirstName = "Mike",
                BirthDate = new DateTime(1982, 3, 17),
                YearResult = 14
            });
        }

        [TestMethod]
        public void GetCount()
        {
            Assert.AreEqual(3, s.Count);
        }
        [TestMethod]
        public void SetName()
        {
            s.Name = "Info";
            Assert.AreEqual("Info", s.Name);
        }

        [TestMethod]
        public void SetNameWithLess3Chars()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => {
                s.Name = "In";
            });
        }
        [TestMethod]
        public void SetNameWithMore10Chars()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => {
                s.Name = "Infdsgfdlkglkhghkghg";
            });
        }

        [TestMethod]
        public void AddStudent()
        {
            int currentCount = s.Count;
            s.Add(new Student { LastName = "Kap", FirstName = "Nathalie", YearResult = 16 });
            Assert.AreEqual(currentCount + 1, s.Count);
        }

        [TestMethod]
        public void AddExistingStudent()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                s.Add(new Student { LastName = "Ly", FirstName = "Khun" });
            });
        }

        [TestMethod]
        public void GetAverage()
        {
            Assert.AreEqual(11.66, s.GetAverage(), 0.01);
        }

        [TestMethod]
        public void GetAverageIfEmpty()
        {
            Section s1 = new Section();
            Assert.AreEqual(0, s1.GetAverage());
        }

        [TestMethod]
        public void GetYoungest()
        {
            Assert.AreEqual("Khun", s.GetYoungest().FirstName);
        }

        [TestMethod]
        public void GetYoungestIfEmpty()
        {
            Section s1 = new Section();
            Assert.AreEqual(null, s1.GetYoungest());
        }

        [TestMethod]
        public void GetYoungestIfSameAge()
        {
            s.Add(new Student { LastName = "Nom", FirstName = "Prenom", BirthDate = new DateTime(1982, 5, 6) });
            Assert.ThrowsException<InvalidOperationException>(() => {
                s.GetYoungest();
            });
        }

        [TestMethod]
        public void GetBestStudents()
        {
            s.Add(new Student { LastName = "Nom", FirstName = "Prenom", BirthDate = new DateTime(1982, 5, 6), YearResult = 14 });
            List<Student> best = s.GetBestStudents();
            Assert.AreEqual(2, best.Count);
            foreach(Student item in best)
            {
                Assert.AreEqual(14, item.YearResult);
            }
        }

        [TestMethod]
        public void GetBestStudentsIfEmpty()
        {
            Section s2 = new Section();
            Assert.AreEqual(0, s2.GetBestStudents().Count);
        }
    }
}
