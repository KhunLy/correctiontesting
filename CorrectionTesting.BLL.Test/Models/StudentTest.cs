﻿using CorrectionTesting.BLL.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionTesting.BLL.Test.Models
{
    [TestClass]
    public class StudentTest
    {
        private Student s;
        
        [TestInitialize]
        public void Setup()
        {
            s = new Student();
        }

        [TestMethod]
        public void SetLastName()
        {
            s.LastName = "Ly";
            Assert.AreEqual("Ly", s.LastName);
        }

        [TestMethod]
        public void SetLastNameWithLessThan2Chars()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
            {
                s.LastName = "a";
            });
        }

        [TestMethod]
        public void SetFirstName()
        {
            s.FirstName = "Ly";
            Assert.AreEqual("Ly", s.FirstName);
        }

        [TestMethod]
        public void SetFirstNameWithLessThan2Chars()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
            {
                s.FirstName = "a";
            });
        }

        [TestMethod]
        public void SetBirthDate()
        {
            s.BirthDate = new DateTime(2000, 1, 1);
            Assert.AreEqual(new DateTime(2000, 1, 1), s.BirthDate);
        }

        [TestMethod]
        public void SetBirthDateWithIncorrectValue()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
            {
                s.BirthDate = DateTime.Now.AddDays(1);
            });
        }

        [TestMethod]
        public void SetYearResult()
        {
            s.YearResult = 15;
        }

        [TestMethod]
        public void SetYearResultWithNegative()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
            {
                s.YearResult = -1;
            });
        }

        [TestMethod]
        public void SetYearResultWithMoreThan20()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() =>
            {
                s.YearResult = 21;
            });
        }

        [TestMethod]
        public void GetEmail()
        {
            s.LastName = "ly";
            s.FirstName = "khun";
            Assert.AreEqual("ly.khun@technobel.be", s.GetEmail());
        }
    }
}
