﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionTesting.BLL.Models
{
    public class Section
    {
		private List<Student> _students;
		private string _name;

		public string Name
		{
			get { return _name; }
			set 
			{
				if (value.Length < 3 || value.Length > 10) 
					throw new ArgumentOutOfRangeException();
				_name = value; 
			}
		}

		public int Count
		{
			get
			{
				return _students.Count;
			}
		}

		public Section()
		{
			_students = new List<Student>();
		}

		public void Add(Student s)
		{
			foreach(Student item in _students)
			{
				if(item.LastName == s.LastName && item.FirstName == s.FirstName)
				{
					throw new ArgumentException();
				}
			}
			_students.Add(s);
		}

		public double GetAverage()
		{
			if (Count == 0) return 0;
			double somme = 0;
			foreach(Student s in _students)
			{
				somme += s.YearResult;
			}
			return somme / Count;
		}

		public Student GetYoungest()
		{
			Student younger = null;
			foreach(Student s in _students)
			{
				if(younger == null || s.BirthDate > younger.BirthDate)
				{
					younger = s;
				}
				else if(s.BirthDate == younger?.BirthDate)
				{
					throw new InvalidOperationException();
				}
			}
			return younger;
		}

		public List<Student> GetBestStudents()
		{
			int max = GetMax();
			List<Student> result = new List<Student>();
			foreach(Student s in _students)
			{
				if(s.YearResult == max)
				{
					result.Add(s);
				}
			}
			return result;
		}

		private int GetMax()
		{
			int max = 0;
			foreach (Student s in _students)
			{
				if(s.YearResult > max)
				{
					max = s.YearResult;
				}
			}
			return max;
		}
	}
}
