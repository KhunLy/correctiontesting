﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorrectionTesting.BLL.Models
{
    public class Student
    {
		private string _lastName;

		public string LastName
		{
			get { return _lastName; }
			set 
			{
				if (value.Length < 2) 
					throw new ArgumentOutOfRangeException();
				_lastName = value; 
			}
		}

		private string _firstName;

		public string FirstName
		{
			get { return _firstName; }
			set 
			{
				if (value.Length < 2)
					throw new ArgumentOutOfRangeException();
				_firstName = value; 
			}
		}

		private DateTime dateTime;

		public DateTime BirthDate
		{
			get { return dateTime; }
			set 
			{
				if (value > DateTime.Now)
					throw new ArgumentOutOfRangeException();
				dateTime = value; 
			}
		}

		private int _yearResult;

		public int YearResult
		{
			get { return _yearResult; }
			set 
			{
				if (value < 0 || value > 20)
					throw new ArgumentOutOfRangeException();
				_yearResult = value; 
			}
		}

		public string GetEmail()
		{
			return (LastName + "." + FirstName + "@technobel.be").ToLower();
		}
	}
}
